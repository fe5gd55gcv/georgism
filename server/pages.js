
module.exports = function (app) {

// #### static pages ####

// Home
app.get('/', async function(req, res){
    res.render(__dirname + "/../public/index.ejs");
});

// If you have funds to donate
app.get('/ifYouHaveFunds', async function(req, res){
    res.render(__dirname + "/../public/ifYouHaveFunds.ejs");
});

// If you have a few minutes
app.get('/aFewMinutes', async function(req, res){
    res.render(__dirname + "/../public/aFewMinutes.ejs");
});

// If you have a few hours
app.get('/aFewHours', async function(req, res){
    res.render(__dirname + "/../public/aFewHours.ejs");
});

// If you have more time
app.get('/ifMoreTime', async function(req, res){
    res.render(__dirname + "/../public/ifMoreTime.ejs");
});

// #### / static pages ####

app.get('/account', async function(req, res){

    var user;
    if (req.session && req.session.user) {
        user = req.session.user;
    }

    // if there is no user then redirect to another page
    if (user == null) {
        res.render(__dirname + "/../public/needLogin.ejs");
        return;
    }

    var organisation;
    console.log(user);
    if (req.session && req.session.user) {
        organisation = await db.collection('organisations').findOne({
            'username' : user.username
        });
        if (1 == 2) {
            //res.redirect('/account');
            //return;
        }
    }

    // if there is no user then redirect to another page
    if (organisation == null) {
        res.redirect("/submitOrganisation");
        return;
    }

    // can remove information that we don't want to send to the client
    if (organisation) {
        delete organisation._id;
        delete organisation.username;
    }

    organisation = JSON.stringify(organisation);
    console.log(organisation);
    //console.log(organisation);

    res.render(__dirname + "/../public/account.ejs", {organisation: organisation});
});

app.get('/organisations', async function(req, res){
    res.render(__dirname + "/../public/organisations.ejs");
});

app.get('/submitOrganisation', async function(req, res, next) { // adding 'next' resolved an error when using res.redirect()

    var user;
    var username;
    var returnUser = {};

    if (req.session && req.session.user) {
        user = req.session.user;
    }

    // check if the user already has an organisation created
    //     if so then redirect them to their organisation page
    if (req.session && req.session.user) {
        var organisation;
        organisation = await db.collection('organisations').findOne({
            'username' : user.username
        });
        if (organisation != null) {
            res.redirect('/account');
            return;
        }
    }
    console.log(organisation);
    /*if (organisation != null) {
        res.redirect(307, '/editOrganisation');
    }*/

    if (user && user != "null") {
        res.render(__dirname + "/../public/submitOrganisation.ejs");
    }
    else {
        res.render(__dirname + "/../public/needLogin.ejs");
    }
});

app.get('/editOrganisation', async function(req, res, next){
    res.render(__dirname + "/../public/editOrganisation.ejs");
});

// redirect page after the user completes the form to submit an organisation
app.get('/thanksForSubmitting', async function(req, res){
    res.render(__dirname + "/../public/thanksForSubmitting.ejs");
});

app.post('/api/addOrganisation', async function(req, res){

    var data = req.body;

    var username;
    if (req.session && req.session.user && req.session.user.username) {
        username = req.session.user.username;
    }

    var time = Date.now();

    /*var name = data.name;
    var website = data.website;
    var email = data.email;
    var firstName = data.firstName;
    var lastName = data.lastName;
    var contactEmail = data.contactEmail;
    var contactPhoneNumber = data.contactPhoneNumber;
    var description = data.description;*/

    // check to make sure that the user is logged in
    // check to make sure that there isn't already an organisation associated with this account

    if (username) {

    var organisationId = ObjectId().toString();
    var organisations;
        organisations = await db.collection('organisations').insertOne({
            '_id': organisationId,
            'username': username,
            'name' : data.name,
            'website' : data.website,
            'email' : data.email,
            'firstName' : data.firstName,
            'lastName' : data.lastName,
            'contactEmail' : data.contactEmail,
            'contactPhoneNumber' : data.contactPhoneNumber,
            'description' : data.description,
            'displayStatus' : false,
            'time' : time
        });

        res.send('Added organisation');
        return;
    }

});

app.post('/api/editOrganisation', async function(req, res){

    var data = req.body;
    var time = Date.now();

    var username;
    if (req.session && req.session.user && req.session.user.username) {
        username = req.session.user.username;
    }

    // check to make sure that the user is logged in
    // check to make sure that the organisation belongs to the user editing it, currently it does not

    var organisation;
    organisation = await db.collection('organisations').findOne({
        'username' : username
    });
    if (organisation == null) {
        res.redirect('/'); // could send an error message
        return;
    }
    
    var organisationId = organisation._id

    var organisations;

    organisations = await db.collection('organisations').updateOne(
        { _id: organisationId },
        { $set:
            {
            'name' : data.name,
            'website' : data.website,
            'email' : data.email,
            'firstName' : data.firstName,
            'lastName' : data.lastName,
            'contactEmail' : data.contactEmail,
            'contactPhoneNumber' : data.contactPhoneNumber,
            'description' : data.description
            }
        }
    );

    res.send('Added organisation');

});

app.post('/g/getOrganisations', async function(req, res, next) {
    // change this to return only the information that should be displayed
    // right now it would leak private information

    var organisations;

    organisations = await db.collection('organisations').find({
        displayStatus: true
    })
    .limit(1000)
    .toArray()
    .then(function(response) {
        return response;
    });

    // https://alligator.io/js/array-sort-numbers/
    organisations = organisations.sort((a, b) => a.time - b.time);
    res.send(JSON.stringify(organisations));

});

app.get('/admin', async function(req, res){
    res.render(__dirname + "/../public/admin.ejs");
});

app.post('/g/getAdminOrganisations', async function(req, res, next) {
    // change this to check to authenticate the user first, they should be logged in as an admin once the login system is built

    var organisations;

    organisations = await db.collection('organisations').find({
    })
    .limit(1000)
    .toArray()
    .then(function(response) {
        return response;
    });

    // https://alligator.io/js/array-sort-numbers/
    organisations = organisations.sort((a, b) => a.time - b.time);
    res.send(JSON.stringify(organisations));

});

app.post('/g/changeOrganisationStatus', async function(req, res, next) {
    // change this to check to authenticate the user first, they should be logged in as an admin once the login system is built

	/*if (!req.session || !req.session.user) {
		res.send('no user');
		return;
	}*/

	//var username = req.session.user.username; // then check if the user is the administrator
	var _id;
	_id = req.body._id;
	var displayStatus;
	displayStatus = req.body.displayStatus;

	await db.collection('organisations').updateOne(
		{
            '_id' : { $in: [ _id, ObjectId(_id) ] } // originally espressjs would create mongodb objects using ObjectId() objects as id's, eventually switched them back to strings, this $in query was to maintain compatibility until switching completely to strings
		},
		{ $set:
			{
				displayStatus: displayStatus
			}
		}
	)
	.then(function (response){
		res.send(response);
		return;
	});

});

// can delete this and corresponding page
app.get('/123', async function(req, res){
    res.render(__dirname + "/../public/123.ejs");
});

// can delete this and corresponding page
app.get('/2', async function(req, res){
    res.render(__dirname + "/../public/index2.ejs");
});

app.get('/map', async function(req, res){
    res.render(__dirname + "/../public/map_test.ejs");
});

}

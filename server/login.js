module.exports = function (app) {


    app.post('/r/getUser', async function(req, res, next) { // not secure enough
		var user;
		var username;
		var returnUser = {};

        if (req.session && req.session.user) {
            user = req.session.user;
        }

		if (user) { // && user._id
        	username = req.session.user.username;
	        checkEmail = await db.collection('users').findOne({
	            'username' : username
	        })
	        .then(function (returnValue){

				// delete sensitive data before sending it back
				//delete returnValue.email;
				//delete returnValue.passwordHash;

				returnUser._id = returnValue._id;
				returnUser.displayName = returnValue.displayName;
				returnUser.username = returnValue.username;

		        res.send(returnUser);
    			res.end(); // maybe use return; instead here
	        });
		}
		else {
			console.log('no user');
			res.send('null');
			res.end();
		}
    });

    app.post('/r/addUser', async function(req, res, next) {
        //console.log(req.body);
        // search to see if user already exists
        var checkUsername;
        checkUsername = await db.collection('users').findOne({
            'username' : req.body.username
        })
        .then(function (returnValue){
            //console.log(returnValue);
            return returnValue;
        });
        
        var checkEmail;
        checkEmail = await db.collection('users').findOne({
            'email' : req.body.email
        })
        .then(function (returnValue){
            //console.log(returnValue);
            return returnValue;
        });
        
        //console.log(checkUsername);
        //console.log(checkEmail);

        // if user exists, go back and say the user already exists
        if (checkUsername != null || checkEmail != null) {
            res.send('Username or email address already exist');
            return;
        }

        // if user doesn't exist, salt and hash password and then add user
        
        var saltString;

        const bcrypt = require('bcrypt');
        const saltRounds = 10;

        var plaintextPassword = req.body.password;
        //console.log(plaintextPassword);
        var hashResult;
        hashResult = await bcrypt.hash(plaintextPassword, saltRounds);

        var userId = ObjectId().toString();

        var newUser;
        newUser = await db.collection('users').insertOne({
            '_id' : userId,
            'username' : req.body.username,
            'email' : req.body.email,
            'passwordHash' : hashResult
        })
        .then(function (returnValue){
            //console.log(returnValue);
            return returnValue;
        });


        req.session.user = {    // put this in a function
            'username' : req.body.username,
            'email' : req.body.email
        };
        req.session.save(); // put this in a function

        // figure out how to use sessions to check for logged-in users
        // https://www.codementor.io/mayowa.a/how-to-build-a-simple-session-based-authentication-system-with-nodejs-from-scratch-6vn67mcy3

        //console.log(newUser);
        // go back and say the user was added redirect to home screen after registering
        //res.end(hashResult);
        res.send('Signed up');
        //res.send(true);

    });


    app.post('/r/login', async function(req, res) {
        var loginFailMessage = "won't log in";
        var loginSuccessMessage = "logged in";
        
        var userFound;
        userFound = await db.collection('users').findOne({
            'username' : req.body.username //,
            //'email' : req.body.email
        })
        .then(function (returnValue){
            return returnValue;
        });
        
        if (userFound == null) {
            res.send(loginFailMessage);
            return;
        }
        
        //console.log(userFound);
        hash = userFound.passwordHash;

        const bcrypt = require('bcrypt');
        passwordMatch = await bcrypt.compare(req.body.password, hash)
        .then(function(res) {
            if (res) {
                console.log('match');
                console.log(res + 'a');
            }
            return res
        });
        console.log(passwordMatch + 'b');
        
        if (passwordMatch) {
            req.session.user = {    // put this in a function
				// only the username is saving because that's the only data being passed from the client, possibly
                //'_id' : req.body._id,
                'username' : req.body.username,
                'email' : req.body.email,
                'displayName' : req.body.displayName,
                'stripeAccountId' : req.body.stripeAccountId
            };
            req.session.save(); // put this in a function
            console.log('success here');
            res.send(loginSuccessMessage);
            return;
        }
        
        console.log('reached end');
        res.send(loginFailMessage);
    });

    app.post('/r/logout', async function(req, res) {
        req.session.destroy();
        res.end();
    });

    // ## / login page ##

}
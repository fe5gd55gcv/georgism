module.exports = function (app) {


    // # connect to database #

	const MongoClient = require('mongodb').MongoClient;
	const assert = require('assert');
	// Connection URL
	//const url = 'mongodb://user1:lppuquq1l0@ds064278.mlab.com:64278/lists1';
	
	//const url;
	var url, dbName;	// this should probably be a const
	url = 'mongodb+srv://user1:n9uucylhiws2o244h9qj@cluster0.icqtv.mongodb.net/?retryWrites=true&w=majority';
	dbName = 'geoism1';

	// Database Name
	//const dbName = 'lists1';

	// Use connect method to connect to the server
	MongoClient.connect(url, 
        //{ useUnifiedTopology: true },
        function(err, client) {
	    assert.equal(null, err);
	    console.log("Connected successfully to server");

	    //const db = client.db(dbName);
	    global.db = client.db(dbName); // maybe this shouldn't be global, make sure it can't be accessed from the client

	    //client.close();
	});

	// https://github.com/Raynos/mongo-col/issues/1
	// use this to find items by their _id, since mongodb puts $oid objects inside the _id field
	// probably shouldn't be global
	global.ObjectId = require('mongodb').ObjectID;

    // # / connect to database #



    // # set up sessions // before the routes because the routes use sessions

    // sessions for login, probably shouldn't be global but I don't want to require it every time
    // https://www.codementor.io/mayowa.a/how-to-build-a-simple-session-based-authentication-system-with-nodejs-from-scratch-6vn67mcy3

    // this didn't work
    var cookieData;
    cookieData = {
        expires: false
    }

    global.session = require('express-session')
    global.MongoStore = require('connect-mongo')(session);
    var sess = {
        secret: 'tsrembexlxaem3yuf7ti', // should this change every time?
        resave: false,
        saveUninitialized: false,
        cookie: cookieData,    // { maxAge: 86400000 } is one day in milliseconds, extend it or make the cookie permanent, { expires: false } makes it not expire
        store: new MongoStore({ url: url }) // https://www.npmjs.com/package/connect-mongo
    }
    if (app.get('env') === 'production') {
        app.set('trust proxy', 1) // trust first proxy
        sess.cookie.secure = true // serve secure cookies
    }
    app.use(session(sess))

    // # / set up sessions

}
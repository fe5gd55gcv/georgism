// Favour ease of following and readibility over efficiency and function.
// People who contribute might not be experienced.


// ## application setup ##

var express = require("express");
var app = express();
var http = require('http').createServer(app);
http.listen(process.env.PORT || 3000, function(){
	console.log('listening on *:3000 or on ' + process.env.PORT);
});
// 'process.env.PORT || ' was added to fix an error
// Error R10 (Boot timeout) -> Web process failed to bind to $PORT within 60 seconds of launch
// https://stackoverflow.com/questions/15693192/heroku-node-js-error-web-process-failed-to-bind-to-port-within-60-seconds-of
app.use(express.json()); // {limit : "1mb"}
app.use(express.urlencoded({
	limit:"1mb",
	extended: false
}));
// set view engine to ejs
app.set('view engine', 'ejs');
// to be able to serve files from the /public/ folder
// https://stackoverflow.com/questions/34073076/node-js-express-framework-cant-get-css-file
app.use(express.static(__dirname + '/public'));

// ## / application setup ##

// ## connect to database ##
require('./server/databaseAndSessions.js')(app);
// ## / connect to database ##

// ## pages ##
require('./server/pages.js')(app);
// ## / pages ##

// ## login ##
require('./server/login.js')(app);
// ## / login ##



